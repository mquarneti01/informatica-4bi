/* scheda esercizi
 * es 3
 */

#include <cmath>
#include <iostream>

using namespace std;

typedef struct {
    int x, y;
} punto;

typedef struct {
    punto p1, p2;
} segmento;

float lunghezzaSegmento(segmento);

int main()
{
    int n;

    cout << "Quanti segmenti vuoi inserire? ";
    cin >> n;

    segmento segmenti[n];

    // acquisizione dati
    for (int i = 0; i < n; i++) {
        cout << "Inserisci x1 e y1" << endl;
        cin >> segmenti[i].p1.x >> segmenti[i].p1.y;

        cout << "Inserisci x2 e y2" << endl;
        cin >> segmenti[i].p2.x >> segmenti[i].p2.y;
    }

    cout << "Calcolo lunghezza segmenti" << endl;

    // stampa risultati
    for (int i = 0; i < n; i++) {
        cout << "(" << segmenti[i].p1.x << "; " << segmenti[i].p1.y << "), "
             << "(" << segmenti[i].p2.x << "; " << segmenti[i].p2.y << ") --> "
             << lunghezzaSegmento(segmenti[i]) << endl;
    }

    return 0;
}

float lunghezzaSegmento(segmento a)
{
    return sqrtf(powf((a.p1.x - a.p2.x), 2) + powf((a.p1.y - a.p2.y), 2));
}
