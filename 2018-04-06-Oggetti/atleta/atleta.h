#ifndef ATLETA_H
#define ATLETA_H

class Atleta {
public:
    Atleta();
    Atleta(int);
    Atleta(Atleta&); // costruttore di copia
    ~Atleta(); // distruttore
    int qualeNumero();

private:
    int pettorina;
};

#endif // ATLETA_H
