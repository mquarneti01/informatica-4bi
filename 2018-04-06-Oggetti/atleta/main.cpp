#include "atleta.h"
#include <iostream>

using namespace std;

int main()
{
    Atleta primo(6);
    Atleta secondo(14);
    Atleta terzo(secondo);

    Atleta gareggianti[10];

    cout << endl
         << primo.qualeNumero() << endl;
    cout << endl
         << secondo.qualeNumero() << endl;
    cout << endl
         << terzo.qualeNumero() << endl;

    gareggianti[0] = primo;
    cout << endl
         << gareggianti[0].qualeNumero() << endl;

    gareggianti[0] = secondo;
    cout << endl
         << gareggianti[0].qualeNumero() << endl;

    {
        Atleta quarto;
        cout << endl
             << "Dentro parentesi" << endl;
    } // qui si distrugge l'oggetto "quarto" perche' si esce dallo scope

    cout << endl;

    return 0;
}
