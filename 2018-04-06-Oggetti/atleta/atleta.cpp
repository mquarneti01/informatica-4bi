#include "atleta.h"
#include <iostream>

using namespace std;

Atleta::Atleta()
{
    cout << "Instanziato oggetto Atleta" << endl;
}

Atleta::Atleta(int numero)
{
    pettorina = numero;

    cout << "Instanziato oggetto Atleta" << endl;
}

Atleta::Atleta(Atleta& altro)
{
    pettorina = 10 + altro.pettorina;

    cout << "Instanziato oggetto Atleta" << endl;
}

Atleta::~Atleta()
{
    cout << "Distrutto oggetto Atleta" << endl;
}

int Atleta::qualeNumero()
{
    return pettorina;
}
