/*
intero "b" da input
intero obbligatoriamente positivo "e" da input
eleva "b" alla "e" tramite algoritmo ricorsivo 
*/

#include <stdio.h>

void potenza(int, int, int*);

int main()
{
    int b, risultato = 1;
    unsigned int e;

    printf("Inserisci b: ");
    scanf("%d", &b);
    printf("Inserisci e: ");
    scanf("%d", &e);

    potenza(b, e, &risultato);
    printf("%d^%d = %d\n", b, e, risultato);
}

void potenza(int a, int b, int* ris)
{
    if (b != 0) {
        *ris *= a;
        potenza(a, b - 1, ris);
    }
}
