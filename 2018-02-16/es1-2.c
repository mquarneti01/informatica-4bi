/*
intero "b" da input
intero obbligatoriamente positivo "e" da input
eleva "b" alla "e" tramite algoritmo ricorsivo 
*/

#include <stdio.h>

int potenza(int, int);

int main()
{
    int b;
    unsigned int e;

    printf("Inserisci b: ");
    scanf("%d", &b);
    printf("Inserisci e: ");
    scanf("%d", &e);

    printf("%d^%d = %d\n", b, e, potenza(b, e));
}

int potenza(int a, int b)
{
    if (b == 0)
        return 1;
    else
        return (a * potenza(a, b - 1));
}
