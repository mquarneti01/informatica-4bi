#include <stdio.h>

int somma(int[], int);

int main()
{
    int n;

    do {
        printf("Inserisci un intero n: ");
        scanf("%d", &n);
    } while (n <= 0);

    int array[n];

    for (int i = 0; i < n; i++) {
        printf("Inserisci l'intero nella posizione %d: ", i);
        scanf("%d", &array[i]);
    }

    printf("Somma: %d\n", somma(array, n));
}

int somma(int a[], int dim)
{
    if (dim > 0)
        return (a[dim - 1] + somma(a, dim - 1));

    return 0;
}
