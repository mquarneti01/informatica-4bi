#include <stdio.h>

int main()
{
    float velocita, coeff, spazio;

    printf("Inserisci la velocita'\n");
    scanf("%f", &velocita);

    printf("Inserisci il coefficiente relativo alle condizioni stradali'\n");
    printf("Asfalto ruvido (0.6), liscio (0.5), bagnato (0.4), ghiacciato (0.1)");
    scanf("%f", &coeff);

    spazio = velocita * velocita / 250 * coeff;

    printf("Lo spazio e' %f\n", spazio);
    return 0;
}
