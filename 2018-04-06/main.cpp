#include "time.h"
#include <iostream>

using namespace std;

int main()
{
    int h, m, s;
    Time tempo;

    cout << "Inserisci le ore" << endl;
    cin >> h;
    cout << "Inserisci i minuti" << endl;
    cin >> m;
    cout << "Inserisci i secondi" << endl;
    cin >> s;

    tempo.setTime(h, m, s);

    cout << endl
         << "Tempo in formato standard" << endl;
    tempo.printTimeStandard();

    cout << endl
         << "Tempo in formato militare" << endl;
    tempo.printTimeMilitar();
}
