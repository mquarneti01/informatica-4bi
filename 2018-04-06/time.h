#ifndef TIME_H
#define TIME_H

class Time {
public:
    void setTime(int, int, int);
    void printTimeStandard();
    void printTimeMilitar();

private:
    int hours;
    int minutes;
    int seconds;
};

#endif // TIME_H
