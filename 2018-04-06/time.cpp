#include "time.h"
#include <iostream>

using namespace std;

void Time::setTime(int h, int m, int s)
{
    hours = h;
    minutes = m;
    seconds = s;
}

void Time::printTimeStandard()
{
    if (hours < 12) {
        cout << hours << ":" << minutes << ":" << seconds << " AM" << endl;
    } else if (hours > 12) {
        cout << hours - 12 << ":" << minutes << ":" << seconds << " PM" << endl;
    } else {
        cout << hours << ":" << minutes << ":" << seconds << " PM" << endl;
    }
}

void Time::printTimeMilitar()
{
    cout << (hours < 10 ? "0" : "") << hours
         << (minutes < 10 ? "0" : "") << minutes
         << (seconds < 10 ? "0" : "") << seconds
         << endl;
}
