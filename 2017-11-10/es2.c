#include <stdio.h>

int main()
{
    int a, b, c;

    printf("Inserisci 3 numeri interi separati da spazi\n");
    scanf("%d %d %d", &a, &b, &c);

    printf("%s\n",
        (c * c == a * a + b * b) ? "E' una terna pitagorica"
                                 : "Non e' una terna pitagorica");
}
