#include <stdio.h>

int main()
{
    int a, b, c, d, e, minimo, media = 0, mediano;

    printf("Inserisci 5 interi separati da spazi\n");
    scanf("%d %d %d %d %d", &a, &b, &c, &d, &e);

    if (a < b && a < c && a < d && a < e)
        minimo = a;
    else if (b < a && b < c && b < d && b < e)
        minimo = a;
    else if (c < a && c < b && c < d && c < e)
        minimo = a;
    else if (d < a && d < b && d < c && d < e)
        minimo = a;
    else
        minimo = e;

    printf("Minimo: %d\n", minimo);

    if (a < b && b < c && c < d && d < e)
        mediano = c;
    if (b < c && c < d && d < e && e < a)
        mediano = d;
    if (c < d && d < e && e < a && a < b)
        mediano = e;
    if (d < e && e < a && a < b && b < c)
        mediano = a;
    if (e < a && a < b && b < c && c < d)
        mediano = b;

    printf("Mediano: %d\n", mediano);
}
