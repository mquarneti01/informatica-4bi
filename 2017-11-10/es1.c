#include <stdio.h>

int main()
{
    char char_1, char_2, char_3, char_4;

    printf("Inserisci 4 caratteri separati da spazi\n");
    scanf("%c %c %c %c", &char_1, &char_2, &char_3, &char_4);

    if (char_1 == char_2)
        printf("%c %c\n", char_1, char_1);
    if (char_1 == char_3)
        printf("%c %c\n", char_1, char_1);
    if (char_1 == char_4)
        printf("%c %c\n", char_1, char_1);
    if (char_2 == char_3)
        printf("%c %c\n", char_2, char_2);
    if (char_2 == char_4)
        printf("%c %c\n", char_2, char_2);
    if (char_3 == char_4)
        printf("%c %c\n", char_3, char_3);
}
