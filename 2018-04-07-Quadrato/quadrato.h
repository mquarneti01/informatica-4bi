#ifndef QUADRATO_H
#define QUADRATO_H

class Quadrato {
    float lato;

public:
    Quadrato(float);
    float getPerimetro();
    float getArea();
};

#endif // QUADRATO_H
