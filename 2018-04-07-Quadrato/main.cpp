#include "quadrato.h"
#include <iostream>

using namespace std;

int main()
{
    cout << "Instanziazione quadrato con lato 10" << endl;
    Quadrato quadrato1(10);
    cout << "Perimetro: " << quadrato1.getPerimetro() << ", area: " << quadrato1.getArea() << endl
         << endl;

    cout << "Instanziazione quadrato con lato 20" << endl;
    Quadrato quadrato2(20);
    cout << "Perimetro: " << quadrato2.getPerimetro() << ", area: " << quadrato2.getArea() << endl;
    return 0;
}
