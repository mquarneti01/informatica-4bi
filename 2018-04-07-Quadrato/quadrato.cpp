#include "quadrato.h"

Quadrato::Quadrato(float l)
{
    lato = l;
}

float Quadrato::getPerimetro()
{
    return (lato * 4);
}

float Quadrato::getArea()
{
    return (lato * lato);
}
