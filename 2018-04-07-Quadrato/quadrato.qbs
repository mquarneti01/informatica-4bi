import qbs

    Project
{
minimumQbsVersion:
    "1.7.1"

        CppApplication
    {
    consoleApplication:
        true files : ["main.cpp",
                         "quadrato.cpp",
                         "quadrato.h",
        ]

                     Group { // Properties for the produced executable
                         fileTagsFilter:
                             "application" qbs.install : true
                     }
    }
}
