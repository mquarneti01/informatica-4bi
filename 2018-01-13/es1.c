#include <stdio.h>

int multipli(int, int);

int main()
{
    int n, m, num;

    printf("Iserisci n e m separati da uno spazio\n");
    scanf("%d %d", &n, &m);
    num = multipli(n, m);
    printf("\nNumero di numeri: %d", num);
}

int multipli(int a, int b)
{
    int j = 0;

    for (int i = 1; i != a; i++) {
        if (i % b == 0) {
            printf("%d - ", i);
            j++;
        }
    }
    return j;
}
