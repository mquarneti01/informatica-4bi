#include <stdio.h>
#define N 15

int main()
{
    int a[N], b[N], c[N];

    for (int i = 0; i < N; i++) {
        printf("Inserisci un elemento di a: ");
        scanf("%d", &a[i]);
    }

    for (int i = 0; i < N; i++) {
        printf("Inserisci un elemento di b: ");
        scanf("%d", &b[i]);
    }

    for (int i = 0; i < N; i++)
        c[i] = a[i] + b[N - i - 1];

    printf("|\ta\t|\tb\t|\tc\t|\n");

    for (int i = 0; i < N; i++)
        printf("|\t%d\t|\t%d\t|\t%d\t|\n", a[i], b[i], c[i]);
}
