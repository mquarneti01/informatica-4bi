#ifndef LIBRO_H
#define LIBRO_H

class Libro {
    int isbn;
    char categoria;
    char genere;
    float prezzo;

public:
    Libro();
    Libro(int, float);
    ~Libro();

    int getISBN();
    void setISNB(int);

    char getCategoria();
    void setCategoria(char);

    char getGenere();
    void setGenere(char);

    float getPrezzo();
    void setPrezzo(float);
};

#endif // LIBRO_H
