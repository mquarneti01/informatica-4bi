#include "libro.h"
#include <iostream>

using namespace std;

int main()
{
    Libro libro1(6546496, 15);
    libro1.setCategoria('r');
    libro1.setGenere('a');

    Libro libro2(9684625, 20);
    libro2.setCategoria('s');
    libro2.setGenere('n');

    Libro libro3(6402117, 11.50);
    libro3.setCategoria('m');
    libro3.setGenere('t');

    cout << "Libro 1 (" << libro1.getCategoria() << ";" << libro1.getGenere() << ") --> " << libro1.getPrezzo() << " Euro" << endl;
    cout << "Libro 2 (" << libro2.getCategoria() << ";" << libro2.getGenere() << ") --> " << libro2.getPrezzo() << " Euro" << endl;
    cout << "Libro 3 (" << libro3.getCategoria() << ";" << libro3.getGenere() << ") --> " << libro3.getPrezzo() << " Euro" << endl;

    return 0;
}
