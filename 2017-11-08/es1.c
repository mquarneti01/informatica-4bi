#include <stdio.h>

int main()
{
    int scelta;

    printf("Menu' di prova\n\n");
    printf("[1] Per immettere dati\n");
    printf("[2] Per determinare il maggiore\n");
    printf("[3] Per determinare il minore\n");
    printf("[4] Per ordinare\n");
    printf("[5] Per visualizzare\n");

    scanf("%d", &scelta);

    if (scelta == 1 || scelta == 2 || scelta == 3 || scelta == 4 || scelta == 5)
        printf("In esecuzione opzione %d\n", scelta);
    else
        printf("Opzione inesistente\n");

    /*
Si può fare anche utilizzando uno switch
switch (scelta)
{
    case 1:
        printf("In esecuzione opzione 1\n");
        break;
    case 2:
        printf("In esecuzione opzione 2\n");
        break;
    case 3:
        printf("In esecuzione opzione 3\n");
        break;
    case 4:
        printf("In esecuzione opzione 4\n");
        break;
    case 5:
        printf("In esecuzione opzione 5\n");
        break;
    default:
        printf("Opzione inesistente\n");
}
*/
}
