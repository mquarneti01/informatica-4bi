/*-
 * prodotto dei numeri pari in un array
 * utilizzando una funzione ricorsiva
 */

#include <stdio.h>

int prodotto(int*, int);

int main()
{
    int n;

    do {
        printf("Inserisci n: ");
        scanf("%d", &n);
    } while (n <= 0);

    int array[n];

    for (int i = 0; i < n; i++) {
        printf("Inserisci l'intero in posizione %d: ", i);
        scanf("%d", &array[i]);
    }

    int p = prodotto(array, n - 1);

    printf("Il prodotto e' %d\n", p);
}

int prodotto(int* array, int dim)
{
    if (!dim) {
        return array[0];
    } else {
        if (dim % 2 == 0) {
            return array[dim] * prodotto(array, dim - 1);
        } else {
            return prodotto(array, dim - 1);
        }
    }
}
