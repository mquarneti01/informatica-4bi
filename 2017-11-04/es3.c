#include <stdio.h>

int main()
{
    int intero;
    char carattere;
    float reale;

    scanf("%d %c %f", &intero, &carattere, &reale);
    printf("intero vale %d, carattere vale %c, reale vale %f\n",
        intero,
        carattere,
        reale);
}
