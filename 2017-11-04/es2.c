// SI PUO' LAVORARE CON LA MEMORIA SOLO SE L'ACCOUNT E' AMMINISTRATORE

#include <stdio.h>

int main()
{
    int *a, b;

    b = 8;
    *a = 5;

    printf("b vale %d e il valore puntato da a vale %d\n", b, *a);

    a = &b;
}
