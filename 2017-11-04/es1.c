// utilizzo dei puntatori delle variabili
// SI PUO' LAVORARE CON LA MEMORIA SOLO SE L'ACCOUNT E' AMMINISTRATORE

int main()
{
    int a = 5;
    int* b = &a; // assegnamo a b (puntatore a interi) l'indirizzo della variabile a
    int c = *b; // assegnamo a c il contenuto della variabile nell'indirizzo
    // contenuto in b, quindi il valore di a

    char d;
    char* e = &d; // puntatore a caratteri

    *e = 'a'; // assegnamo alla variabile il cui indirizzo e' contenuto in e (cioè
        // d) il valore 'a'
}
