#include <stdio.h>

int main()
{
    int prova;

    printf("Inserisci un numero di prova: ");
    scanf("%d", &prova);

    if (prova > 100)
        prova--; // se l'istruzione e' una sola le parentesi graffe si possono
    // omettere

    prova > 100 ? printf("Si'\n")
                : printf("No\n"); // uso piu' compatto di if-else
    printf("Prova ora vale %d\n", prova);
}
