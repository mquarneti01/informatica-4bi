/*
Si scriva un programma che,utilizzi una
funzione appositamente sviluppata, converta
un numero binario in un valore decimale
Il numero binario e' rappresentato su n bit,
con N inserito da tastiera cosi' come il
numero da convertire. L'utente inserira' il
numero una cifra alla volta, partendo dal
meno significativo. In uscita verra' stampato
il numero decimale ottenuto dalla conversione
*/

#include <math.h>
#include <stdio.h>

int kawaii(int);

int main()
{
    int N, ris;

    printf("Quanto deve essere lungo il numero da convertire\n");
    scanf("%d", &N);
    ris = kawaii(N);
    printf("Il numero convertito e' pari a: %d\n", ris);
    return 0;
}

kawaii(int lung)
{
    int numero, i = 0, convert = 0, j;

    j = lung - 1;
    while (i < lung) {
        printf("Inserisci una cifra del numero binario partendo dal piu' significativo\n");
        scanf("%d", &numero);
        convert += numero * (pow(2, j));
        j--;
        i++;
    }
    return convert;
}
