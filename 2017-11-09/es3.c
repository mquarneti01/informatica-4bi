#include <stdio.h>

int main()
{
    float prezzo_iniziale, prezzo_finale;

    printf("Inserisci il prezzo iniziale\n");
    scanf("%f", &prezzo_iniziale);

    if (prezzo_iniziale < 750) {
        prezzo_finale = prezzo_iniziale - prezzo_iniziale * 0.03;
    } else if (prezzo_iniziale >= 1000) {
        prezzo_finale = prezzo_iniziale - prezzo_iniziale * 0.05;
    } else {
        prezzo_finale = prezzo_iniziale;
        printf("Eh no lo sconto non te lo becchi\n");
    }
    printf("Il prezzo finale e' %f\n", prezzo_finale);
}
