#include <stdio.h>

int main()
{
    float persona_1, persona_2, persona_3, media;

    printf("Inserisci le eta' di tre persone (separate da spazi)\n");
    scanf("%f %f %f", &persona_1, &persona_2, &persona_3);

    media = (persona_1 + persona_2 + persona_3) / 3;

    printf("Media = %f", media);
}
