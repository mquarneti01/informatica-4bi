#include <stdio.h>

int main()
{
    float tot_iscritti, tot_votanti, tot_si, tot_no, percent_votanti, percent_si,
        percent_no;

    printf("Inserisci il numero degli iscritti a votare\n");
    scanf("%d", &tot_iscritti);
    printf("Inserisci il numero dei votanti\n");
    scanf("%d", &tot_votanti);
    printf("Inserisci il numero dei si'\n");
    scanf("%d", &tot_si);
    printf("Inserisci il numero dei no\n");
    scanf("%d", &tot_no);

    percent_votanti = tot_votanti / tot_iscritti * 100;
    percent_si = tot_si / tot_votanti * 100;
    percent_no = 100 - percent_si;

    printf("Percentuale votanti sul totale degli iscritti: %f\n",
        percent_votanti);
    printf("Percentuale si': %f\n", percent_si);
    printf("Percentuale no: %f\n", percent_no);
}
