#include <math.h>
#include <stdio.h>

float funzione(int);

int main()
{
    int x;

    printf("Inserisci x\n");
    scanf("%d", &x);
    printf("%f", funzione(x));
}

float funzione(int x)
{
    float y = (float)x;

    return 3 * pow(y, 3) - sqrt(pow(y, 2) + 3) / 2;
}
