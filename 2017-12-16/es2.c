#include <stdio.h>

int x;

void f()
{
    int x;

    x = 1;

    {
        int x;
        x = 2;
    }

    printf("%d\n", x);

    {
        // int x;
        x = 2;
    }

    printf("%d\n", x);

    x = 3;

    printf("%d\n", x);
}

int main()
{
    scanf("%d", &x);
    f();
    printf("%d\n", x);
}
