// Esempio di utilizzo di abs() e pow()

#include <math.h>
#include <stdio.h>

main()
{
    int a, b, segmento, lunghezza, seconda, doppia_lunghezza;

    printf("\n\nLUNGHEZZA SEGMENTO\n");
    printf("Primo estremo: ");
    scanf("%d", &a);
    printf("Secondo estremo: ");
    scanf("%d", &b);

    segmento = a - b;

    lunghezza = abs(segmento);
    doppia_lunghezza = 2 * abs(segmento);
    seconda = pow(lunghezza, 2);

    printf("Lunghezza segmento: %d\n", lunghezza);
    printf("Doppia lunghezza segmento: %d\n", doppia_lunghezza);
    printf("Lunghezza segmento alla seconda: %d\n", seconda);
}
