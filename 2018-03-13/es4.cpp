#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    char name[50];

    cout << "Digita il tuo nome: ";
    cin >> name;

    cout << "Ciao, " << name << endl;
    return 0;
}
