/* calcolo numero fattoriale */

#include <stdio.h>

int fattoriale(int);

int main()
{
    int n;

    printf("Inserisci un intero\n");
    scanf("%d", &n);

    printf("Fattoriale: %d", fattoriale(n));
}

int fattoriale(int a)
{
    /*
    int fat = 1, i;

    for (i = 1; i < a + 1; i++) {
        fat *= i;
    }

    return fat;
    */

    // OPPURE

    if (a < 0)
        return -1;
    if (a == 0)
        return 1;
    else
        return a * fattoriale(a - 1);
}
