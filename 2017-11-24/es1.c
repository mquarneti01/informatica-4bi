#include <stdio.h>

int main()
{
    int num, somma = 0, prodotto = 1;

    while (somma <= 1000) {
        printf("Inserisci un numero intero\n");
        scanf("%d", &num);
        somma += num;
        prodotto *= num;
    }
    printf("\nProdotto: %d\n", prodotto);
}
