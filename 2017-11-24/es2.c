#include <stdio.h>

int main()
{
    int consecutivi = 1, num_1, num_2;

    printf(
        "Il programma si ferma se inserisci due numeri consecutivi uguali\n\n");

    printf("Inserisci un numero: ");
    scanf("%d", &num_1);

    while (consecutivi) {
        printf("Inserisci un numero: ");
        scanf("%d", &num_2);

        if (num_1 == num_2)
            consecutivi = 0;
        else
            num_1 = num_2;
    }
}
