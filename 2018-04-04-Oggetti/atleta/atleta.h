#ifndef ATLETA_H
#define ATLETA_H

class Atleta {
public:
    int pettorina;

    Atleta();

    Atleta(int);

    Atleta(Atleta&); // costruttore di copia

    int qualeNumero();
};

#endif // ATLETA_H
