/*
 * Verifica se una stringa contiene un carattere
 */

#include <iostream>

using namespace std;

bool occorrenza(char[], int, char);

int main()
{
    int n;

    cout << "Inserisci n: ";
    cin >> n;

    char stringa[n];

    int i;
    for (i = 0; i < n; i++) {
        cout << "Inserisci il carattere " << i << ": ";
        cin >> stringa[i];
    }

    char carattere;
    cout << "Inserisci il carattere da cercare: ";
    cin >> carattere;

    bool var = occorrenza(stringa, n, carattere);

    cout << endl
         << var << endl;

    return 0;
}

bool occorrenza(char stringa[], int dim, char carattere)
{
    if (dim < 0)
        return false;

    if (stringa[dim] == carattere)
        return true;
    else
        return occorrenza(stringa, dim - 1, carattere);
}
