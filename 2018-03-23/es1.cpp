/* scheda esercizi
 * es 2
 */

#include <iostream>

using namespace std;

typedef struct {
    int x, y;
} punto;

typedef struct {
    punto p1, p2;
} segmento;

int main()
{
    int n;

    cout << "Quanti segmenti vuoi inserire? ";
    cin >> n;

    segmento segmenti[n];

    for (int i = 0; i < n; i++) {
        cout << "Inserisci x1 e y1" << endl;
        cin >> segmenti[i].p1.x >> segmenti[i].p1.y;

        cout << "Inserisci x2 e y2" << endl;
        cin >> segmenti[i].p2.x >> segmenti[i].p2.y;
    }

    return 0;
}
