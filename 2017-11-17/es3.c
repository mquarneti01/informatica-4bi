// Uso di fflush e do-while
#include <stdio.h>

int main()
{
    char carattere;
    int a = 1;

    do {
        printf("Inserisci un cararattere\n");
        scanf("%c", &carattere);

        if (((carattere >= 'a') && (carattere <= 'z')) || ((carattere >= 'A') && (carattere <= 'Z')))
            printf("%c: %d\n", carattere, carattere);
        else
            printf("Carattere non valido\n");

        fflush(stdin); // pulisce l'input
    } while (carattere != '0');
}
