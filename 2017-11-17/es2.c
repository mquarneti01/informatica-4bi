#include <stdio.h>

int main()
{
    int mese, giorno;

    printf("Digita il numero del mese\n");
    scanf("%d", &mese);

    switch (mese) {
    case 1:
    case 2:
        printf("Inverno\n");
        break;
    case 3:
        printf("Inserisci il giorno\n");
        scanf("%d", &giorno);
        printf("%s\n", giorno < 21 ? "Inverno" : "Primavera");
        break;
    case 4:
    case 5:
        printf("Primavera");
        break;
    case 6:
        printf("Inserisci il giorno\n");
        scanf("%d", &giorno);
        printf("%s\n", giorno < 21 ? "Primavera" : "Estate");
        break;
    case 7:
    case 8:
        printf("Estate");
        break;
    case 9:
        printf("Inserisci il giorno\n");
        scanf("%d", &giorno);
        printf("%s\n", giorno < 21 ? "Estate" : "Autunno");
        break;
    case 10:
    case 11:
        printf("Autunno");
        break;
    case 12:
        printf("Inserisci il giorno\n");
        scanf("%d", &giorno);
        printf("%s\n", giorno < 21 ? "Autunno" : "Inverno");
    }
}
