#include <stdio.h>

void aree(int, int, float*, float*);

void main()
{
    int b = 4, h = 10;
    float ret = 0, tri = 0;

    printf("Prima: b = %d, h = %d, ret = %f, tri = %f\n\n", b, h, ret, tri);

    aree(b, h, &ret, &tri);

    printf("Dopo: b = %d, h = %d, ret = %f, tri = %f\n\n", b, h, ret, tri);
}

void aree(int base, int altezza, float* areaR, float* areaT)
{
    *areaR = base * altezza;
    *areaT = (float)(base * altezza) / 2;
}
