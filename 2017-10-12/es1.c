#include <stdio.h>

int main()
{
    int base, altezza, area;
    int var1, var2, var3, var4;
    char carattere = 'a';

    var1 = sizeof(base);
    var2 = sizeof(altezza);
    var3 = sizeof(area);
    var4 = sizeof(carattere);

    printf("AREA RETTANGOLO\n\n");

    printf("Valore base: ");
    scanf("%d", &base);

    printf("Valore altezza: ");
    scanf("%d", &altezza);

    area = base * altezza;

    printf("\nBase: %d\n", base);
    printf("Altezza: %d\n", altezza);
    printf("Area: %d\n", area);

    printf("\nDimensione \"base\": %d\n", var1);
    printf("Dimensione \"altezza\": %d\n", var2);
    printf("Dimensione \"area\": %d\n", var3);
    printf("Dimensione \"carattere\": %d\n", var4);
    return 0;
}
