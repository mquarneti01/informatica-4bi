#include <stdio.h>

void scambio(int*, int*);

int main()
{
    int a, b;

    printf("Inserisci due interi separati da uno spazio\n");
    scanf("%d %d", &a, &b);

    scambio(&a, &b);

    printf("%d %d", a, b);
}

void scambio(int* a, int* b)
{
    int c = *a;

    *a = *b;
    *b = c;
}
