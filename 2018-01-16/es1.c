#include <stdio.h>

float funzione(float, float);

int main()
{
    float a, b, x;

    printf("Inserisci a e b separati da uno spazio\n");
    scanf("%f %f", &a, &b);

    x = funzione(a, b);

    printf("x = %f", x);
}

float funzione(float a, float b)
{
    return -1 * b / a;
}
