#include <stdio.h>

void bubble(int[], int);
void scambia(int*, int*);

int main()
{
    int numeri[] = { 15, 4, 1, 7, 3, 2, 5 }, size;

    size = sizeof(numeri) / sizeof(numeri[0]);

    bubble(numeri, size);

    for (int i = 0; i < size; i++) {
        printf("%d ", numeri[i]);
    }
}

void bubble(int vettore[], int dim)
{
    int scambio = 1, i;

    while (scambio) {
        scambio = 0;
        i = 0;
        while (i < dim - 1) {
            if (vettore[i] > vettore[i + 1]) {
                scambia(&vettore[i], &vettore[i + 1]);
                scambio = 1;
            }
            i++;
        }
    }
}

void scambia(int* a, int* b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}
