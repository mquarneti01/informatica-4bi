/*
 * Saluta l'utente, chiede due interi
 * e ne visualizza la somma
 */

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int somma(int, int);

int main()
{
    char name[50];

    cout << "Digita il tuo nome: ";
    cin >> name;

    cout << "Ciao, " << name << endl;

    int a, b;
    cout << "Inserisci due numeri separati da uno spazio" << endl;
    cin >> a >> b;

    int sum = somma(a, b);

    cout << endl
         << "Somma: " << sum << endl;

    return 0;
}

int somma(int a, int b)
{
    return a + b;
}
