#include <iostream>

using namespace std;

char myVar = 'A';

int main()
{
    char myVar;

    cout << "Valore di myVar: " << myVar << endl;

    myVar = 'Z';

    cout << "Valore di myVar: " << myVar << endl;

    return 0;
}
