/*
 * Dati da tastiera un numero intero <= di 50 e > 0, accettato con
 * un controllo sull'input ed un passo di incremento/decremento intero,
 * stampi la sequenza dei numeri maggiori di 0 e minori di 100
 * partendo dal numero dato
 */

#include <iostream>

using namespace std;

int main()
{
    int num, passo;

    do {
        cout << "Inserisci un intero <= 50: ";
        cin >> num;
    } while (num <= 0 || num > 50);

    cout << "Inserisci un intero (passo): ";
    cin >> passo;

    while (num > 0 && num <= 100) {
        cout << num << endl;
        num += passo;
    }

    return 0;
}
