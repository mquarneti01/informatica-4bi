#ifndef ACQUATICO_H
#define ACQUATICO_H

#include "animale.h"

class Acquatico : public Animale {
public:
    Acquatico();
    ~Acquatico();

    void respira();
};

#endif // ACQUATICO_H
