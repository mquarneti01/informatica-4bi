#include "terrestre.h"
#include <iostream>

using std::string;
using std::cout;
using std::endl;

Terrestre::Terrestre() {}

Terrestre::Terrestre(Animale original) {
    this->bocca = original.getBocca();
    this->numZampe = original.getNumZampe();
    this->numAli = original.getNumAli();
    this->numPinne = 0;
    this->numOcchi = original.getNumOcchi();
    this->habitat = 'T';
    this->scheletro = original.getScheletro();
    this->esterno = original.getEsterno();
    this->nome = original.getNome();
}

Terrestre::~Terrestre() {}

void Terrestre::respira() {
    cout << "Un animale terrestre respira ossigeno." << endl;
}

void Terrestre::setNome(string val) {
    this->nome = val + " che balla";
}

void Terrestre::copiaNome(Terrestre ogg) {
    this->nome = ogg.getNome();
}
