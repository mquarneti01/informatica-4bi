#ifndef ANIMALE_H
#define ANIMALE_H

#include <string>
using std::string;

class Animale {

protected:
    string nome;
    int numZampe; // 0 o più
    int numAli; // 0 o più
    int numPinne; // 0 o più
    int numOcchi; // 0 o più
    char habitat; // T (terra), A (aria), W (acqua)
    char bocca; // D (dentata), B (becco), S (speciale)
    bool scheletro; // true, false
    char esterno; // S (pelle), M (mucosa), P (piume), C (corazza), F (pelliccia), L (squame)

public:
    Animale();
    Animale(string);
    Animale(char, char, bool);
    ~Animale();

    string getNome();
    void setNome(string);

    int getNumZampe();
    void setNumZampe(int);

    int getNumAli();
    void setNumAli(int);

    int getNumPinne();
    void setNumPinne(int);

    int getNumOcchi();
    void setNumOcchi(int);

    char getHabitat();
    void setHabitat(char);

    char getBocca();
    void setBocca(char);

    bool getScheletro();
    void setScheletro(bool);

    char getEsterno();
    void setEsterno(char);
};

#endif // ANIMALE_H
