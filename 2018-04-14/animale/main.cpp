#include "terrestre.h"
#include "animale.h"
#include <iostream>

using namespace std;

int main()
{
    Animale torterra("Torterra");
    torterra.setHabitat('T');
    torterra.setEsterno('F');
    torterra.setScheletro(true);
    torterra.setNumZampe(4);
    torterra.setNumOcchi(2);
    torterra.setBocca('B');

    Animale magikarp("Magikarp");
    magikarp.setHabitat('W');
    magikarp.setEsterno('L');
    magikarp.setScheletro(true);
    magikarp.setNumPinne(4);
    magikarp.setNumOcchi(2);
    magikarp.setBocca('S');

    Animale pidgeotto('A', 'P', true);
    pidgeotto.setNome("Pidgeotto");
    pidgeotto.setNumZampe(2);
    pidgeotto.setNumAli(2);
    pidgeotto.setNumOcchi(2);
    pidgeotto.setBocca('B');

    // cout << boolalpha << true << endl; // questo serve per stampare "true" invece che 1

    cout << magikarp.getNumZampe() << endl;

    Terrestre terr1(torterra);
    terr1.respira();

    return 0;
}
