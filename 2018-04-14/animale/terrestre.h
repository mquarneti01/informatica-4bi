#ifndef TERRESTRE_H
#define TERRESTRE_H

#include "animale.h"


class Terrestre : public Animale
{
    public:
        Terrestre();
        Terrestre(Animale);
        ~Terrestre();

        void respira();
        void setNome(string);
        void copiaNome(Terrestre);
};

#endif // TERRESTRE_H
