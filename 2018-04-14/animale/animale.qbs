import qbs
Project {
    minimumQbsVersion: "1.7.1"

    CppApplication {
        consoleApplication: true
        files : ["acquatico.cpp",
            "acquatico.h",
            "animale.cpp",
            "animale.h",
            "main.cpp",
            "terrestre.cpp",
            "terrestre.h",
        ]

        Group { // Properties for the produced executable
            fileTagsFilter: "application"
            qbs.install : true
        }
    }
}
