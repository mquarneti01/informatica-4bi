#include "animale.h"

Animale::Animale() {}
Animale::Animale(string n) { nome = n; }
Animale::Animale(char h, char e, bool s)
{
    habitat = h;
    esterno = e;
    scheletro = s;
}

Animale::~Animale() {}

string Animale::getNome() { return nome; }
void Animale::setNome(string var) { nome = var; }

int Animale::getNumZampe() { return numZampe; }
void Animale::setNumZampe(int numZampe) { this->numZampe = numZampe; } // this -> indica l'oggetto della classe

int Animale::getNumAli() { return numAli; }
void Animale::setNumAli(int var) { (*this).numAli = var; } // (*this). è uguale a this ->

int Animale::getNumPinne() { return numPinne; }
void Animale::setNumPinne(int var) { numPinne = var; }

int Animale::getNumOcchi() { return numOcchi; }
void Animale::setNumOcchi(int var) { numOcchi = var; }

char Animale::getHabitat() { return habitat; }
void Animale::setHabitat(char var) { habitat = var; }

char Animale::getBocca() { return bocca; }
void Animale::setBocca(char var) { bocca = var; }

bool Animale::getScheletro() { return scheletro; }
void Animale::setScheletro(bool var) { scheletro = var; }

char Animale::getEsterno() { return esterno; }
void Animale::setEsterno(char var) { esterno = var; }
