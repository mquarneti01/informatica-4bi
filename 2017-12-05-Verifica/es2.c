// Esercizio 2

#include <stdio.h>

int main()
{
    int n, test, var;

    // immissione di n e test
    printf("Inserisci il numero di interi da immettere: ");
    scanf("%d", &n);
    printf("Inserisci un intero \"test\": ");
    scanf("%d", &test);
    printf("\n\n");

    for (; n != 0; n--) {
        printf("Inserisci un intero: ");
        scanf("%d", &var);

        // verifica dell'intero inserito
        if (var < test)
            printf("L'intero inserito e' minore di test\n");
        else if (var > test)
            printf("L'intero inserito e' maggiore di test\n");
        else
            printf("L'intero inserito e' uguale a test\n");

        // stampa del carattere ASCII
        printf("Il corrispondente carattere ASCII e' %c\n\n", var);
    }
}
