// Esercizio A

#include <stdio.h>

// definizione delle costanti dei prezzi
#define INTERO 7.50
#define RIDOTTO 5.00
#define PENSIONATI 5.50
#define MILITARI 5.50

int main()
{
    char biglietto;
    int tot_i = 0, tot_r = 0, tot_p = 0, tot_m = 0;
    float incasso_i, incasso_r, incasso_p, incasso_m;

    // stampa del menu
    printf("Intero          [I] --> %f Euro\n", INTERO);
    printf("Ridotto         [R] --> %f Euro\n", RIDOTTO);
    printf("Pensionati      [P] --> %f Euro\n", PENSIONATI);
    printf("Militari        [M] --> %f Euro\n", MILITARI);
    printf("Fine immissione [F]\n");

    do {
        // immissione del tipo di biglietto
        printf("Inserisci il tipo di biglietto\n");
        scanf("%c", &biglietto);
        fflush(stdin);

        // verifica del tipo di biglietto e aumento del conteggio
        switch (biglietto) {
        case 'i':
        case 'I':
            tot_i++;
            break;
        case 'r':
        case 'R':
            tot_r++;
            break;
        case 'p':
        case 'P':
            tot_p++;
            break;
        case 'm':
        case 'M':
            tot_m++;
        }
    } while (
        (biglietto != 'f') && (biglietto != 'F')); // continua se il carattere e' diverso da 'f' e 'F'

    // calcolo degli incassi
    incasso_i = tot_i * INTERO;
    incasso_r = tot_r * RIDOTTO;
    incasso_p = tot_p * PENSIONATI;
    incasso_m = tot_m * MILITARI;

    // stampa dei conteggi e degli incassi
    printf("\n\nConteggio\n");
    printf("Biglietti interi:         %2d --> %10f Euro\n", tot_i, incasso_i);
    printf("Biglietti ridotti:        %2d --> %10f Euro\n", tot_r, incasso_r);
    printf("Biglietti per pensionati: %2d --> %10f Euro\n", tot_p, incasso_p);
    printf("Biglietti per militari:   %2d --> %10f Euro\n", tot_m, incasso_m);
    printf("Totale:                   %2d --> %10f Euro\n",
        tot_i + tot_r + tot_p + tot_m,
        incasso_i + incasso_r + incasso_p + incasso_m);
}
