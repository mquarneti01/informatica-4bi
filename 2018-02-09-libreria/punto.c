#include "punto.h"

punto trasla(punto p, int a, int b)
{
    punto tmp;

    tmp.x = p.x + a;
    tmp.y = p.y + b;

    return tmp;
}

punto proietta_x(punto p)
{
    punto tmp;

    tmp.x = p.x;
    tmp.y = 0;

    return tmp;
}

punto proietta_y(punto p)
{
    punto tmp;

    tmp.x = 0;
    tmp.y = p.y;

    return tmp;
}
