#include "punto.h"
#include <stdio.h>

int main()
{
    punto p, p2;
    int a, b;

    printf("Inserisci X(p): ");
    scanf("%d", &p.x);

    printf("Inserisci Y(p): ");
    scanf("%d", &p.y);

    printf("\nDi quanto vuoi traslare i punto nell'asse X? ");
    scanf("%d", &a);
    printf("Di quanto vuoi traslare i punto nell'asse Y? ");
    scanf("%d", &b);

    p2 = trasla(p, a, b);
    printf("\nTraslato: %d;%d\n", p2.x, p2.y);

    p2 = proietta_x(p);
    printf("Proiettato nell'asse X: %d;%d\n", p2.x, p2.y);

    p2 = proietta_y(p);
    printf("Proiettato nell'asse Y: %d;%d\n", p2.x, p2.y);
}
