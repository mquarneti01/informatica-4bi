#ifndef RETTANGOLO_H
#define RETTANGOLO_H

class Rettangolo {
    float base;
    float altezza;

public:
    Rettangolo();
    Rettangolo(float, float);
    ~Rettangolo();

    float getBase();
    float setBase(float);
    float getAltezza();
    float setAltezza(float);

    float getDiagonale();
};

#endif // RETTANGOLO_H
