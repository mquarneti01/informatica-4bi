#include "rettangolo.h"
#include <cmath> // da sistemare: non funziona

Rettangolo::Rettangolo() {}
Rettangolo::~Rettangolo() {}

Rettangolo::Rettangolo(float b, float a)
{
    base = b;
    altezza = a;
}

float Rettangolo::getBase() { return base; }
float Rettangolo::setBase(float var) { base = var; }

float Rettangolo::getAltezza() { return altezza; }
float Rettangolo::setAltezza(float var) { altezza = var; }

float Rettangolo::getDiagonale()
{
    return (sqrt(pow(base, 2) + pow(altezza, 2)));
}
