#include "rettangolo.h"
#include <iostream>

using namespace std;

int main()
{
    Rettangolo rett(3, 5);
    cout << rett.getBase() << endl
         << rett.getAltezza() << endl;
    cout << endl
         << rett.getDiagonale();
}
