// Uso degli indirizzi di memoria

#include <stdio.h>

int main()
{
    int a = 5;
    int b = &a; // & e' l'indirizzo in memoria
    int c = 7;
    int* d; // variabile di tipo puntatore ad intero

    *d = 8;

    printf("La varabile di valore %d", a);
    printf(" ha indirizzo %d\n", b);
    printf("Il prodotto di a*c vale %d\n", a * c);
}
