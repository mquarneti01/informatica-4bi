/*
 * inserire citta e temperature massima e minima
 * calcola la media
 */

#include <iostream>

using namespace std;

typedef struct {
    string name;
    float maxTemp;
    float minTemp;
} city;

int main()
{
    int numeroCitta = 10;
    city citta[numeroCitta];

    for (int i = 0; i < numeroCitta; i++) {
        cout << "Inserisci una citta' (oppure 0 se vuoi terminare l'inserimento)" << endl;
        cin >> citta[i].name;
        if (citta[i].name != "0") {
            cout << "Insersci la temperatura massima" << endl;
            cin >> citta[i].minTemp;
            cout << "Insersci la temperatura minima" << endl;
            cin >> citta[i].maxTemp;
            i++;
        } else {
            numeroCitta = i - 1;
        }
    }

    cout << endl
         << "Temperature medie:" << endl;

    for (int i = 0; i < numeroCitta; i++) {
        cout << citta[i].name << " --> " << (citta[i].minTemp + citta[i].maxTemp) / 2 << endl;
    }

    return 0;
}
