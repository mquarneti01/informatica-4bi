/*
stampa al contrario una stringa data in input
deve utilizzare una funzione ricorsiva
*/

#include <stdio.h>
#define DIM 16

void ftnirp(char*, int);

int main()
{
    char stringa[DIM];

    printf("Inserisci una stringa\n");
    scanf("%s", stringa);

    ftnirp(stringa, DIM);
}

void ftnirp(char* stringa, int dim)
{
    if (stringa[dim] && stringa[dim] != '\0')
        printf("%c", stringa[dim]);

    if (!dim)
        return;

    ftnirp(stringa, dim - 1);
}
