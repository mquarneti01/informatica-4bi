#include <stdio.h>

int abs(int);

int main()
{
    int num;

    printf("Inserisci un intero\n");
    scanf("%d", &num);
    num = abs(num);
    printf("Valore assoluto: %d", num);
}

int abs(int a)
{
    if (a < 0)
        return -a;
    return a;
}
