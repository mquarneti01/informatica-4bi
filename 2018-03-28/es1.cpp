/*
 * scheda esercizi c++ no oggetti
 * es 5
 */

#include <iostream>

using namespace std;

typedef struct {
    string nome, cognome, matricola;
    float reddito;
} studente;

studente fill_student();
void print_results(studente);
int taxes(float);

int main()
{
    int n; // numero studenti

    cout << "Quanti studenti vuoi inserire? ";
    cin >> n;

    studente student_array[n];

    for (int i = 0; i < n; i++) {
        cout << "Studente " << i + 1 << endl;
        student_array[i] = fill_student();
    }

    for (int i = 0; i < n; i++)
        print_results(student_array[i]);

    return 0;
}

studente fill_student()
{
    studente student;

    cout << "Nome: ";
    cin >> student.nome;
    cout << "Cognome: ";
    cin >> student.cognome;
    cout << "Matricola: ";
    cin >> student.matricola;
    cout << "Reddito nucleo famigliare: ";
    cin >> student.reddito;
    cout << endl;

    return student;
}

void print_results(studente student)
{
    cout << student.nome << " " << student.cognome
         << " (" << student.matricola << ") --> Laurea "
         << (student.matricola[0] == 'L' ? "triennale" : "specialista") << endl
         << "Importo da pagare: " << taxes(student.reddito) << " Euro" << endl;
}

int taxes(float reddito)
{
    if (reddito < 20000)
        return 250;
    else if (reddito < 30000)
        return 500;
    else
        return 1000;
}
