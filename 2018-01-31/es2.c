/*
Scrivere un programma che implementi una
funzione che, dopo aver acquisito nel programma
principale due array di interi da input detti
v1 e v2 di lunghezza minima di 5 elementi,
stampi in output la differenza v1 - v2,
ovvero gli elementi di v1 che non siano in v2
*/

#include <stdio.h>
#define LUNGHEZZA 5

void diff(int[], int[]);

int main()
{
    int v1[LUNGHEZZA], v2[LUNGHEZZA];

    printf("Inserimento primo array di interi\n\n");
    for (int i = 0; i < LUNGHEZZA; i++) {
        printf("Inserisci l'elemento %d: ", i);
        scanf("%d", &v1[i]);
    }

    printf("\n\nInserimento secondo array di interi\n\n");
    for (int i = 0; i < LUNGHEZZA; i++) {
        printf("Inserisci l'elemento %d: ", i);
        scanf("%d", &v2[i]);
    }

    diff(v1, v2);
}

void diff(int a[], int b[])
{
    int ripetuto, i, j;

    for (i = 0; i < LUNGHEZZA; i++) {
        ripetuto = 0;
        j = 0;
        while (j < LUNGHEZZA && !ripetuto) {
            if (a[i] == b[j])
                ripetuto = 1;
            j++;
        }
        if (!ripetuto)
            printf("%d ", a[i]);
    }
}
