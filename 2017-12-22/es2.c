#include <stdio.h>

int val = 6;

void funProva();

int main()
{
    printf("%d\n", val);

    printf("Inserisci un valore: ");
    scanf("%d", &val);

    funProva();

    printf("%d\n", val);
}

void funProva()
{
    int val = 4;

    printf("Qui val vale %d\n", val);
}
