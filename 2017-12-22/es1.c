#include <stdio.h>

int verifica_decimale(float);
int verifica_ascii(int);

int main()
{
    float valore;

    do {
        printf("Inserisci un numero intero\n");
        scanf("%f", &valore);
    } while (verifica_decimale(valore));

    if (verifica_ascii(valore))
        printf("Corrisponde ad un codice ASCII\n");
    else
        printf("Non corrisponde ad un codice ASCII\n");
}

int verifica_decimale(float a)
{
    if ((int)a != a)
        return 1;
    return 0;
}

int verifica_ascii(int a)
{
    if (a >= 0 && a <= 256)
        return 1;
    return 0;
}
