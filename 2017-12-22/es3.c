#include <stdio.h>

void fun(int x, int* p)
{
    x = 23;
    *p = 45;
}

int main()
{
    int a, b;
    int* q;

    a = 1;
    b = 15;

    printf("Prima: a = %d, b = %d\n", a, b);
    fun(a, &b);
    printf("Dopo: a = %d, b = %d\n", a, b);

    a = 1;
    b = 15;
    q = &b;

    printf("Prima: a = %d, b = %d, q = %p\n",
        a,
        b,
        q); //%p stampa l'indirizzo in memoria
    fun(a, &b);
    printf("Dopo: a = %d, b = %d, q = %p\n", a, b, q);
}
