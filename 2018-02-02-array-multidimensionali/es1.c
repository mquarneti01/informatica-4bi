#include <stdio.h>
#define N 3
#define M 3

void inserisci(int[][M], int);
void stampa(int[][M], int);

int main()
{
    int matrice[N][M];

    inserisci(matrice, N);
    stampa(matrice, N);
}

void inserisci(int matrice[][M], int dim)
{
    int i, j;

    for (i = 0; i < N; i++) {
        for (j = 0; j < M; j++) {
            printf("Inserisci il valore nella riga %d, colonna %d\n", i, j);
            scanf("%d", &matrice[i][j]);
        }
        printf("\n");
    }
}

void stampa(int matrice[][M], int dim)
{
    int i, j;

    printf("\n\n");
    for (i = 0; i < N; i++) {
        printf("| ");
        for (j = 0; j < M; j++)
            printf("%8d |", matrice[i][j]);
        printf("\n");
    }
}
