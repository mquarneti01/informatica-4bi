/*
Calcolo area rettangolo
*/

// questo è un commento ad una riga

#include <stdio.h>

#define BASE 3
#define ALTEZZA 7

int main()
{
    int area = BASE * ALTEZZA;

    printf("Base: %d, Altezza: %d\nArea: %d\n", BASE, ALTEZZA, area);
    return 0;
}
