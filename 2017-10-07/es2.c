/*
Calcolo area rettangolo
*/

// questo è un commento ad una riga

#include <stdio.h>

int main()
{
    const int BASE = 3, ALTEZZA = 7;
    int area = BASE * ALTEZZA;

    printf("Base: %d, Altezza: %d\nArea: %d\n", BASE, ALTEZZA, area);
    return 0;
}
