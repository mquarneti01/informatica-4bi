#include <stdio.h>

int main()
{
    int n, intero, tot_10 = 0, tot_100 = 0, tot_1000 = 0;

    printf("Quanti interi vuoi inserire?\n");
    scanf("%d", &n);

    for (; n != 0; n--) {
        printf("Inserisci un intero: ");
        scanf("%d", &intero);

        switch (intero) {
        case 10:
            tot_10++;
            break;
        case 100:
            tot_100++;
            break;
        case 1000:
            tot_1000++;
        }
    }
    printf("\n\n10 e' stato inserito %d volte\n", tot_10);
    printf("100 e' stato inserito %d volte\n", tot_100);
    printf("1000 e' stato inserito %d volte\n", tot_1000);
}
