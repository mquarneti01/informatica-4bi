/*
lette le coordinate di un punto P,
applica a P tramite funzioni:
trastazione, proiezione secondo asse x e asse y
*/

#include <stdio.h>

typedef struct {
    int x, y;
} punto;

punto trasla(punto, int, int);
punto proietta_x(punto);
punto proietta_y(punto);

int main()
{
    punto p, p2;
    int a, b;

    printf("Inserisci X(p): ");
    scanf("%d", &p.x);

    printf("Inserisci Y(p): ");
    scanf("%d", &p.y);

    printf("\nDi quanto vuoi traslare i punto nell'asse X? ");
    scanf("%d", &a);
    printf("Di quanto vuoi traslare i punto nell'asse Y? ");
    scanf("%d", &b);

    p2 = trasla(p, a, b);
    printf("\nTraslato: %d;%d\n", p2.x, p2.y);

    p2 = proietta_x(p);
    printf("Proiettato nell'asse X: %d;%d\n", p2.x, p2.y);

    p2 = proietta_y(p);
    printf("Proiettato nell'asse Y: %d;%d\n", p2.x, p2.y);
}

punto trasla(punto p, int a, int b)
{
    punto tmp;

    tmp.x = p.x + a;
    tmp.y = p.y + b;

    return tmp;
}

punto proietta_x(punto p)
{
    punto tmp;

    tmp.x = p.x;
    tmp.y = 0;

    return tmp;
}

punto proietta_y(punto p)
{
    punto tmp;

    tmp.x = 0;
    tmp.y = p.y;

    return tmp;
}
