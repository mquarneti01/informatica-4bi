#include <stdio.h>

int main()
{
    char string1[32], string2[16];
    int i = 0;

    printf("Inserisci una stringa\n");
    scanf("%s", &string1);
    printf("Inserisci un'altra stringa\n");
    scanf("%s", &string2);

    while (string1[i])
        i++;

    for (int j = 0; j != 5; j++) {
        string1[i] = string2[j];
        i++;
    }

    printf("\nStringa finale: %s\n", string1);
}
