#include <stdio.h>

int main()
{
    int days, hours, minutes, seconds;

    printf("Inserisci giorni, ore, minuti e secondi separati da spazi\n");
    scanf("%d %d %d %d", &days, &hours, &minutes, &seconds);

    printf("Secondi totali: %d\n",
        days * 86400 + hours * 3600 + minutes * 60 + seconds);
}
