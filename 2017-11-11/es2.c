// && ha precedenza su ||
#include <stdio.h>

int main()
{
    float lato_1, lato_2, lato_3;

    printf("Inserisci i tre lati di un triangolo (separati da spazi)\n");
    scanf("%f %f %f", &lato_1, &lato_2, &lato_3);

    if (lato_1 == lato_2 && lato_1 == lato_3)
        printf("Il triangolo e' equilatero\n");
    else if (lato_1 == lato_2 || lato_1 == lato_3 || lato_2 == lato_3)
        printf("Il triangolo e' isoscele\n");
    else
        printf("Il triangolo e' scaleno\n");
}
