/* controlla l'occorrenza di un carattere in una stringa */

#include <stdio.h>

int main()
{
    int n;
    printf("Inserisci n: ");
    scanf("%d", &n);

    char stringa[n];
    printf("Inserisci una stringa lunga %d\n", n);
    scanf("%s", &stringa);
    fflush(stdin);

    char c;
    printf("Inserisci un carattere: ");
    scanf("%c", &c);

    int contatore = 0;
    for (int i = 0; i < n; i++) {
        if (stringa[i] == c) {
            contatore++;
        }
    }

    printf("\n%c appare %d volte\n", c, contatore);
}
