#include <stdio.h>
#define NUM 4

void scambio(int*, int*);

int main()
{
    int int_array[NUM];

    for (int i = 0; i != NUM; i++) {
        printf("Inserisci l'intero della posizione %d: ", i);
        scanf("%d", &int_array[i]);
    }

    for (int i = 0; i != NUM * 2; i += 2)
        scambio(&int_array[i], &int_array[i + 1]);

    printf("Array finale: ");

    for (int i = 0; i != NUM; i++)
        printf("%d ", int_array[i]);
}

void scambio(int* a, int* b)
{
    int c = *a;

    *a = *b;
    *b = c;
}
