/*
Programma che, tramite una funzione, riceve
in input due numeri complessi (cioe' una coppia
di valori rappresentanti la parte reale e quella
immaginaria) e restituisce la loro somma (anch'essa
una coppia di valori)
*/

#include <stdio.h>

void complex_sum(float, float, float, float, float*, float*);

int main()
{
    float a[2], b[2], c[2];

    printf("Inserisci un numero complesso nel formato [a + bi]\n");
    scanf("%f + %fi", &a[0], &a[1]);
    printf("Inserisci un altro numero complesso nel formato [a + bi]\n");
    scanf("%f + %fi", &b[0], &b[1]);

    complex_sum(a[0], a[1], b[0], b[1], &c[0], &c[1]);

    printf("\nSomma: %f + %fi\n", c[0], c[1]);
}

void complex_sum(float ar, float ai, float br, float bi, float* cr, float* ci)
{
    *cr = ar + br;
    *ci = ai + bi;
}
