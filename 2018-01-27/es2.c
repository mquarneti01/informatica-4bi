/*
Sistema di cronometraggio per la Formula 1
registra i tempi in milliscondi. Tuttavia
tali tempi devono essere presentati in
termini di minuti, secondi e millisecondi.
Creare un programma che, sfruttando una
funzione appositamente realizzata, riceva
in ingresso un tempo dato in millisecondi
e restituisca l'equivalente in termini di
minuti, secondi e millisecondi.
*/

#include <stdio.h>

void calcola(int*, int*, int*);

int main()
{
    int min, sec, msec;

    printf("Inserisci il tempo in millisecondi\n");
    scanf("%d", &msec);

    calcola(&min, &sec, &msec);

    printf("%d:%d:%d\n", min, sec, msec);
}

void calcola(int* min, int* sec, int* msec)
{
    *sec = *msec / 1000;
    *msec = *msec % 1000;
    *min = *sec / 60;
    *sec = *sec % 60;
}
