#include <stdio.h>
#define X 25
#define Y 80

void printxy(char, int, int);

int main()
{
    int x, y;
    char var;

    printf("Inserisci il carattere da stampare\n");
    scanf("%c", &var);

    printf("Inserisci x\n");
    scanf("%d", &x);

    printf("Inserisci y\n");
    scanf("%d", &y);

    printxy(var, x, y);
}

void printxy(char c, int x, int y)
{
    int i = 0, j = 0;

    for (; i < x; i++)
        printf("\n");
    for (; j < y; j++)
        printf(" ");

    printf("%c", c);

    for (; i < X; i++)
        printf("\n");
    for (; j < Y; j++)
        printf(" ");
}
