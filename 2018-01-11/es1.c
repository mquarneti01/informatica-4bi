#include <stdio.h>
#define ARRAY_CAPACITY 15

int main()
{
    int var;
    int array[ARRAY_CAPACITY];

    // riempie l'array
    for (int i = 0; i < ARRAY_CAPACITY; i++) {
        printf("Inserisci l'intero nella posizione %d: ", i);
        scanf("%d", &var);
        array[i] = var;
    }

    // stampa l'array
    for (int i = 0; i < ARRAY_CAPACITY; i++)
        printf("%d ", array[i]);
}
