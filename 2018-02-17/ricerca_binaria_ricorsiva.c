#include <stdio.h>

int binary_search(int[], int, int, int);

int main()
{
    int n;
    printf("Quanti interi vuoi inserire?\n");
    scanf("%d", &n);

    int array[n];
    for (int i = 0; i < n; i++) {
        printf("Inserisci l'intero numero %d: ", i);
        scanf("%d", &array[i]);
    }

    int val;
    printf("Valore da cercare: ");
    scanf("%d", &val);

    printf("Il valore e' nella posizione %d\n", binary_search(array, 0, n - 1, val));
}

int binary_search(int vet[], int first, int last, int el)
{
    int med;

    if (first > last)
        return -1;

    med = (first + last) / 2;

    if (el == vet[med])
        return med;
    if (el > vet[med])
        return binary_search(vet, med + 1, last, el);
    else
        return binary_search(vet, first, med - 1, el);
}
