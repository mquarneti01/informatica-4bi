#include <stdio.h>

int binary_search(int[], int, int);

int main()
{
    int n;
    printf("Quanti interi vuoi inserire?\n");
    scanf("%d", &n);

    int array[n];
    for (int i = 0; i < n; i++) {
        printf("Inserisci l'intero numero %d: ", i);
        scanf("%d", &array[i]);
    }

    int val;
    printf("Valore da cercare: ");
    scanf("%d", &val);

    printf("Il valore e' nella posizione %d\n", binary_search(array, n, val));
}

int binary_search(int array[], int size, int var)
{
    int p = 0, r = size - 1;

    if (var < array[p] || var > array[r])
        return -1;

    while (p <= r) {
        int q = (p + r) / 2;

        if (array[q] == var)
            return q;
        if (array[q] > var)
            r = q - 1;
        else
            p = q + 1;
    }

    return -1;
}
