#include <math.h>
#include <stdio.h>

int main()
{
    float a;

    printf("Inserisci un numero con virgola mobile\n");
    scanf("%f", &a);

    printf("%f approssimato per difetto = %d\n", a, (int)floor(a));
    printf("%f approssimato per eccesso = %d\n", a, (int)ceil(a));
}
