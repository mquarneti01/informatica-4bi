#include <math.h>
#include <stdio.h>

int main()
{
    float a;

    printf("Inserisci un numero positivo\n");
    scanf("%f", &a);

    for (int i = 1; i <= 5; i++)
        printf("%f eleveto alla %d = %f\n", a, i, pow(a, i));
}
