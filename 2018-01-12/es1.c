// esercizio 6

#include <math.h>
#include <stdio.h>

float area_cerchio(float);
float area_quadrato(float);

int main()
{
    int scelta;
    float var, area;

    printf("Raggio di un cerchio [0] o lato di un quadrato [1]?\n");
    scanf("%d", &scelta);

    printf("Inserisci il %s: ",
        scelta ? "lato del quadrato" : "raggio del cerchio");

    scanf("%f", &var);

    if (var < 0) {
        printf("Errore: valore negativo");
        return 1;
    }

    area = scelta ? area_quadrato(var) : area_cerchio(var);

    printf("Area = %f\n", area);
}

float area_cerchio(float raggio)
{
    return M_PI * pow(raggio, 2);
}

float area_quadrato(float lato)
{
    return pow(lato, 2);
}
