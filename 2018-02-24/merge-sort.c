#include <stdio.h>

void merge_sort(int[], int, int);
void merge(int[], int, int, int, int);

int main()

    void merge_sort(int a[], int p, int r)
{
    int q;

    if (p < r) {
        q = (p + r) / 2;
        merge_sort(a, p, q);
        merge_sort(a, q + 1, r);
        merge(/* non ho fatto in tempo a copiare */);
    }
}

void merge(int a[], int p, int q, int r)
{
    int b[MAX], i, j, k;

    for (i = p, j = q + 1, b = p; i <= q && j <= r;)
        if (a[i] < a[j])
            b[k++] = a[i++];
        else
            b[k++] = a[j++];
    while (i <= q)
        b[k++] = a[i++];
    while (j <= r)
        b[k++] = a[j++];
    for (k = p; k <= r; k++)
        a[k] = b[k];
}
