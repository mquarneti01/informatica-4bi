#include <stdio.h>

int maggiore(int, int);
int minore(int, int);
void sequenza(int, int);

int main()
{
    int m, n;

    printf("Inserisci m: ");
    scanf("%d", &m);
    printf("Inserisci n: ");
    scanf("%d", &n);

    printf("\nMaggiore: %d\n", maggiore(m, n));
    printf("Minore: %d\n\n", minore(m, n));
    printf("Sequenza: ");
    sequenza(m, n);
}

int maggiore(int a, int b)
{
    if (a >= b)
        return a;
    else
        return b;
}

int minore(int a, int b)
{
    if (a <= b)
        return a;
    else
        return b;
}

void sequenza(int a, int b)
{
    if (a <= b) {
        while (a <= b) {
            printf("%4d", a);
            a++;
        }
    } else {
        while (a >= b) {
            printf("%4d", a);
            a--;
        }
    }
}
