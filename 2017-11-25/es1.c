#include <math.h>
#include <stdio.h>

int main()
{
    float y;

    while (1) {
        printf("Inserisci un numero reale positivo\n");
        scanf("%f", &y);

        if (y >= 0)
            break;
        else
            printf("Il numero inserito e' negativo\n");
    }

    int x = ceil(y);
    while (pow(x, x) > y)
        x--;

    printf("\nx e' %d\n", x);
}
