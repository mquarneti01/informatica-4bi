/*
ES 2.5
Eseguire le 4 operazioni tra due numeri inseriti da tastiera
*/

#include <stdio.h>

main()
{
    float num1, num2;

    printf("Inserisci il primo numero\n");
    scanf("%f", &num1);

    printf("Inserisci il secondo numero\n");
    scanf("%f", &num2);

    printf("\nSomma: %f\n", num1 + num2);
    printf("Differenza: %f\n", num1 - num2);
    printf("Prodotto: %f\n", num1 * num2);
    printf("Quoziente: %f\n", num1 / num2);
}
