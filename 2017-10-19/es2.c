/*
ES 2.4
Calcolo della media tra due numeri inseriti da tastiera
*/

#include <stdio.h>

main()
{
    float num1, num2;

    printf("Inserisci il primo numero\n");
    scanf("%f", &num1);

    printf("Inserisci il secondo numero\n");
    scanf("%f", &num2);

    printf("La media e' %f\n", (num1 + num2) / 2);
}
