// Utilizzo di if-else

#include <stdio.h>

main()
{
    int i;

    printf("Inserisci un intero: ");
    scanf("%d", &i);

    if (i < 100)
        printf("Minore di 100\n");
    else
        printf("Maggiore o uguale a 100\n");
}
