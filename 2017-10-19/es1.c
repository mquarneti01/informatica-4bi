/*
ES 2.3
Lettura di un intero e visualizzazione dell'intero precedente e del
successivo
*/

#include <stdio.h>

main()
{
    int intero;

    printf("Inserisci un numero intero\n");
    scanf("%d", &intero);

    printf("Precedente: %d, successivo: %d\n", intero - 1, intero + 1);
}
