/*
 * scheda esercizi
 * es 4
 */

#include <iostream>

using namespace std;

typedef struct {
    string nome, cognome, matricola;
    float reddito;
} studente;

void fill_student_array(studente*, int);
void print_results(studente*, int);

int main()
{
    int n; // numero studenti

    cout << "Quanti studenti vuoi inserire? ";
    cin >> n;

    studente student_array[n];

    fill_student_array(student_array, n);

    print_results(student_array, n);

    return 0;
}

void fill_student_array(studente* student_array, int dim)
{
    for (int i = 0; i < dim; i++) {
        cout << "Studente " << i + 1 << endl;
        cout << "Nome: ";
        cin >> student_array[i].nome;
        cout << "Cognome: ";
        cin >> student_array[i].cognome;
        cout << "Matricola: ";
        cin >> student_array[i].matricola;
        cout << "Reddito nucleo famigliare: ";
        cin >> student_array[i].reddito;
        cout << endl;
    }
}

void print_results(studente* student_array, int dim)
{
    for (int i = 0; i < dim; i++) {
        cout << student_array[i].nome
             << " " << student_array[i].cognome
             << " (" << student_array[i].matricola << ") --> Laurea "
             << (student_array[i].matricola[0] == 'L' ? "triennale" : "specialista")
             << endl;
    }
}
