#include <stdio.h>

int main()
{
    int numeri[5]; // crea array di 5 interi
    int i = 0;

    numeri[3] = 59; // assegna 59 alla 4° posizione

    printf("Inserisci un numero intero: ");
    scanf("%d", &numeri[2]); // inserisce da tastiera nella 3° posizione

    printf("%d\n", numeri[2]); // stampa il valore in 3° posizione

    numeri[3] = numeri[3] + numeri[2]; // aggiunge il valore nella 3° posizione a
    // quello nella 4° posizione e lo salva
    // nella 4°
    printf("%d\n\n", numeri[3]);

    for (i = 0; i < 5; i++)
        printf("%d\n", );
}
