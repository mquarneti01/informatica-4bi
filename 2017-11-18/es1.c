// Uso di for
#include <stdio.h>
#define MAXINT -32767
#define MININT 32768

int main()
{
    int val, min = MININT, max = MAXINT, media = 0, n;

    printf("Inserire la quantita' di interi di cui calcolare massimo, minimo e "
           "media: ");
    scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        printf("Inserire un numero intero: ");
        scanf("%d", &val);

        if (val < min)
            min = val;

        if (val > max)
            max = val;

        media += val;
    }

    if (n > 0)
        media /= n;

    printf("Minimo = %d\nMassimo = %d\nMedia = %d\n", min, max, media);
}
